#include "../includes/slimeet.h"

SlimeEt::SlimeEt():Et()
{
    setDano(1);
}

bool SlimeEt::act()
{
    if( getLife() <= 0 )
    {
        die();
        return true;
    }

    switch (ToolBox::pseudorandom(0 , 4 ))
    {
        case 0:
        case 1:
            goTo( getPosX() + 1 ,getPosY() );
            break;

        case 2:
            goTo(getPosX(),getPosY() + 1);
            break;

        case 3:
        case 4:
            goTo(getPosX() - 1,getPosY());
            break;


    }



    return true;

}
