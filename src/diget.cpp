#include "../includes/diget.h"
#include "../includes/toolbox.h"


DigEt::DigEt()
{
  setDano(1);
}

bool DigEt::act()
{
    if( getLife() <= 0 )
    {
        die();
        return true;
    }

    switch (ToolBox::pseudorandom(0 , 1 ))
    {


        case 0:
            goTo( getPosX() + 1 ,getPosY() + 1 );
            break;
        case 1:
            goTo(getPosX() - 1,getPosY() + 1 );
            break;


    }

    return true;
}
