#include "../includes/air.h"
#include "../includes/entidade.h"

Air::Air():Element()
{

}

Air::~Air()
{

}


bool Air::colision(Element *  ptr )
{
  Entidade* eptr = static_cast< Entidade* >(ptr);

  if( !eptr ) return false;

  eptr->move(getPosX(),getPosY());


  return true;

}
