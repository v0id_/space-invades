#include "../includes/entidade.h"


Entidade::Entidade( int l): Element()
{
    setLife(l);
}

Entidade::~Entidade()
{

}

bool Entidade::move(int x, int y)
{

    return World::getWorld().moveElement(getPosX(),getPosY(),x,y); //ask to the world

}

void Entidade::setLife(int l)
{
    life = l>0 ? l : 0;
}

int Entidade::getLife()
{
    return life;
}

void Entidade::setDano(int d )
{
    dano = d;
}

int Entidade::getDano()
{
    return dano;
}

bool Entidade::goTo(int x, int y)
{
    if(World::getWorld().isOutOfWorld(x,y)) return false;

    Element* object_ptr = World::getWorld().getWorldElementPtr(x,y);

    if(!object_ptr) return false; //if it is Null

    this->colision(object_ptr); //colide com o elemento a frente

    (*object_ptr)>>=(this);



    return true;
}

void Entidade::die()
{
    int x = getPosX();
    int y = getPosY();

    World::getWorld().removeElement(x,y);  //se remove

    World::getWorld().insertElement(new Air,x,y); //insere ar no lugar
    return;
}





