#include "../includes/action.h"

Action::Action(): type(INVALID)
{

}

Action::Action(string agtTpy,TipoAction tp , int agx, int agy, int x, int y)
{
    tipoAgente = agtTpy;

    agtX = agx;

    agtY = agy;

    type = tp;

    objX = x;

    objY = y;

}
