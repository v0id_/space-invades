#include "../includes/et.h"

Et::Et( int l):Entidade(l )
{

}

bool Et::colision(Element * obj_ptr )
{
    if(dynamic_cast< Et* >( obj_ptr )) return false; //se for da mesma especie, nao faz nd

    Entidade* ent_ptr = dynamic_cast< Entidade* >( obj_ptr );

    if(! ent_ptr ) return false; // se nao for uma entidade, nao faz nd

    ent_ptr->setLife(ent_ptr->getLife() - this->getDano());

    return true;
}


