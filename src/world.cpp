#include "../includes/world.h"

#include "../includes/entidade.h"
#include "../includes/air.h"
#include "../includes/nave.h"
#include "../includes/dangerair.h"


World* World::instance = nullptr;


World::World()
{

    for(int i = 0; i < sizeX ; i++)
    {
        for(int j = 0; j < sizeY ; j++)
        {
        map[i][j] = new Air;

        map[i][j]->setPosX(i);
        map[i][j]->setPosY(j);
        }
    }


}



World &World::getWorld()
{
    if( !instance ) instance = new World;

    return *instance;
}

World::~World()
{
    for(int i = 0; i < sizeX ; i++)
        for(int j = 0; j < sizeY ; j++)
        {
            if(map[i][j] != nullptr) delete map[i][j];

        }
}

bool World::airInit()
{

    for(int i = 0; i < sizeX ; i++)
    {
        for(int j = 0; j < sizeY ; j++)
        {
            if(j == sizeY - 1) map[i][j] = new DangerAir();
            else map[i][j] = new Air();

            if(!map[i][j]) return false;

            map[i][j]->setPosX(i);
            map[i][j]->setPosY(j);
        }

    }

    return false;
}

bool World::moveElement(int frX, int frY, int toX, int toY) //THERE
{
    Element* air = map[toX][toY];
    if( !dynamic_cast<Air*>(map[toX][toY])) return false; //you should not pass

    if( isOutOfWorld(frX,frY) || isOutOfWorld(toX,toY)) return false; //check the location

    Element* tmp = map[frX][frY];

     air = map[toX][toY];

    map[frX][frY] = map[toX][toY];
    map[toX][toY] = tmp;

    air = map[frX][frY];

    Action act( (typeid( tmp )).name() , MOVE, frX,frY, toX, toY);
    actionQueue << act;    //insert on the action queue

    tmp->setPosX(toX);
    tmp->setPosY(toY);

    air->setPosX(frX);
    air->setPosY(frY);

    return true;
}

bool World::insertElement(Element* elem_ptr , int x, int y)
{
    if( map[x][y] ) return false; //so insere onde nao tem nada

    elem_ptr->setPosX(x); //define posicao do elemento
    elem_ptr->setPosY(y);


    map[x][y] = elem_ptr; //coloca o elemento

    Entidade* enty_ptr;

    enty_ptr = dynamic_cast< Entidade* >(elem_ptr);




    if( enty_ptr ) entyList.insert(entyList.begin(),enty_ptr); //if its entity, insert on the list






    Action act( (typeid(map[x][y])).name() , INSERT, x, y);
    actionQueue << act;    //insert on the action queue



    return true;
}

void World::removeElement(int x , int y )
{
    if(isOutOfWorld(x,y)) return; //garantia

    if( !map[x][y]  ) return ; //nao remove null

    Entidade* enty_ptr= dynamic_cast< Entidade* >(map[x][y]);

    if( enty_ptr)
    {


        entyList.remove(enty_ptr); //if its entity, remove from the list

    }

    Action act( (typeid(map[x][y])).name(), DELETE, x, y);
    actionQueue << act;    //insert on the action queue


    delete map[x][y];    //deleta o elemento
    map[x][y] = nullptr;


    return;
}

bool World::isOutOfWorld(int x, int y)
{
    return (x >= getSizeX() ||
            y >= getSizeY() ||
            x < 0        ||      y < 0);
}

Element *World::getWorldElementPtr(int i, int j)
{
    return map[i][j];
}

Entidade *World::getEntity(int num)
{
    list<Entidade*>::iterator it = entyList.begin();

    advance(it, num);

    return *it;
}

void World::actAllEnty()
{
    int it;

    for(it = 0; it < numEnty(); it++)
    {
        getEntity(it)->act();


    }
}

void World::killAllEnty()
{
    int num = numEnty();
    for(int i = 0; i < num; i++)
    {
       (*entyList.begin())->die();
    }

}

