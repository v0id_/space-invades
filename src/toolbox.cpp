#include "../includes/toolbox.h"

ToolBox::ToolBox()
{

}


int ToolBox::pseudorandom(int min, int max)
{
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution < int > uniform_dist(min, max);
    return uniform_dist(e1);
}
