#include "includes/bullet.h"

Bullet::Bullet()
{

}

bool Bullet::act()
{
    if(getLife() <= 0 )
    {
        die();
        return true;
    }
    if(getPosY() == 0)
    {
        die(); //no topo da tela
        return true;
    }
    goTo(getPosX(),getPosY() - 1); //sobe

    return true;
}

bool Bullet::colision(Element* elem)
{
     Entidade* enty_ptr = dynamic_cast<Entidade*>(elem);

     if( enty_ptr ) enty_ptr->setLife(enty_ptr->getLife() - getDano()); //causa dano

    return true;
}
