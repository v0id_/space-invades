
#include "overwatcher.h"

#include <QtDebug>
#include <QPainter>

Overwatcher* Overwatcher::instance = nullptr;

Overwatcher &Overwatcher::getInstance()
{
    if( !instance ) instance = new Overwatcher;

    return *instance;
}

Overwatcher::~Overwatcher()
{
    if(viewPtr) delete viewPtr;
    if(player) delete player;
    if(myscore) delete myscore;
}

void Overwatcher::shot(int x)
{
    Entidade* enty = dynamic_cast<Entidade*>(World::getWorld().getWorldElementPtr(x,player->getPosY()-1));
    if( enty )
    {
        enty->die();
        return;
    }

    QElement<Bullet>* blt = new QElement<Bullet>;
    blt->setRect(0,0,40,40);
    QBrush mybrush(Qt::SolidPattern); //muda cor
    mybrush.setColor( Qt::cyan );
    blt->setBrush(mybrush);
    blt->update();


    insert( blt, player->getPosX(), player->getPosY() - 1 );

}

void Overwatcher::startGame()
{
    cont = 0;

    if(myscore) delete myscore;

    startMensage->hide();


    World::getWorld().killAllEnty();

    World::getWorld().airInit();

    player = new QElement<Nave>;
    insert(player, World::getWorld().getSizeX() / 2 , World::getWorld().getSizeY() - 1 );
    QBrush mybrush(Qt::SolidPattern); //muda cor
    mybrush.setColor( Qt::green );
    player->setBrush(mybrush);
    player->update();

    QGraphicsRectItem* canon = new QGraphicsRectItem(player);
    canon->setRect(15,-10,20,40);
    mybrush.setColor( Qt::green );
    canon->setBrush(mybrush);
    canon->update();

    QGraphicsRectItem* left_base = new QGraphicsRectItem(player);
    left_base->setRect(45,15,20,35);
    mybrush.setColor( Qt::green );
    left_base->setBrush(mybrush);
    left_base->update();


    QGraphicsRectItem* right_base = new QGraphicsRectItem(player);
    right_base->setRect(-15,15,20,35);
    mybrush.setColor( Qt::green );
    right_base->setBrush(mybrush);
    right_base->update();


    myscore = new Score; //alocate the score
    addItem(myscore);

    isRunning = true;



    timer.start(cycleTime); //the real start

}

bool Overwatcher::insert(Element *elem, int x, int y)
{
    World::getWorld().removeElement(x,y);

    if (!World::getWorld().insertElement(elem,x,y)) return false;


    QGraphicsItem* graptr = dynamic_cast<QGraphicsItem*>(elem);
    if(!graptr) return false;

    addItem(graptr);

    syncScene(graptr); //converte possicao para a scene

    return true;
}

bool Overwatcher::syncScene(QGraphicsItem* item )
{
    if( !item ) return false;

    Element* elem_ptr = dynamic_cast<Element*>(item);
    if( !elem_ptr ) return false;

    item->setPos(elem_ptr->getPosX()  * width() / World::getWorld().getSizeX() + 20,  //posiciona na cena
                 elem_ptr->getPosY()  * height()/ World::getWorld().getSizeY() );



    return true;
}

void Overwatcher::insertEny()
{
    if(World::getWorld().numEnty() <= entyNumMax)
    {
        int randX=ToolBox::pseudorandom(0,World::getWorld().getSizeX());

        if( randX % 2 == 0)
        {
            QElement<SlimeEt>* elem = new QElement<SlimeEt>;

            QBrush mybrush(Qt::SolidPattern); //muda cor
            mybrush.setColor( Qt::red );
            elem->setBrush(mybrush);
            elem->update();



            insert(elem,randX,0);

       }
       else
       {
            QElement<DigEt>* elem = new QElement<DigEt>;

            QBrush mybrush(Qt::SolidPattern); //muda cor
            mybrush.setColor( Qt::yellow );
            elem->setBrush(mybrush);
            elem->update();

            insert(elem,randX,0);

        }


    }

}

void Overwatcher::endGame()
{

    for(int n = 0; n < World::getWorld().numEnty(); n++)
    {
      QGraphicsItem* item= dynamic_cast<QGraphicsRectItem*>(World::getWorld().getEntity(n));
      if(item->scene() == this ) removeItem(item);
    }





    isRunning = false;

    timer.stop(); //para o jogo


    startMensage->show();



}

bool Overwatcher::isShipDead()
{
    for(int n = 0; n < World::getWorld().numEnty(); n++)
    {
      if(dynamic_cast<Nave*>(World::getWorld().getEntity(n))) return false;
    }

    return true;
}

void Overwatcher::keyPressEvent(QKeyEvent *event)
{
    if(!isRunning && event->key() == Qt::Key_Space)
    {
       startGame();
       return;
    }
    if( !isShipDead() )
    {

        switch ( event->key() )
        {
            case Qt::Key_Left:
                player->goTo(player->getPosX() - 1, player->getPosY());
            break;

            case Qt::Key_Right:
                player->goTo(player->getPosX() + 1, player->getPosY());
            break;

            case Qt::Key_Space:
                shot(player->getPosX());

        }
        syncScene(player);

    }
}



void Overwatcher::actAll()
{
    if(isShipDead()) endGame();
    else {



        World::getWorld().actAllEnty();

        for(int n = 0; n < World::getWorld().numEnty(); n++)
        {
          QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(World::getWorld().getEntity(n));
           syncScene( item );
        }

        cont++;

        if(cont % 2 == 0  )
        {

           insertEny();

           myscore->Increass(); //increass the score
           myscore->update();


        }

    }
}

Overwatcher::Overwatcher():QGraphicsScene(QRect(0, 0, 1200, 720)), cont(0),isRunning(false)
{
    myscore = nullptr;

    connect(&timer,SIGNAL(timeout()),this,SLOT(actAll()));


    viewPtr = new QGraphicsView();
    viewPtr->setScene(this);

    viewPtr->setFixedSize(1202,722); //a bit bigger



    startMensage = new QGraphicsTextItem;
    startMensage->setPlainText(QString("Use <- and -> to move\n  Press space to start\n  Try to not get hit :)"));
    startMensage->setDefaultTextColor(Qt::white);
    startMensage->setFont(QFont("times", 19));
    //
    addItem(startMensage);
    startMensage->setPos(width()/2 - 117,height()/2-80);


    setBackgroundBrush(QColor(10,25,112));





}
