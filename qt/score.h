//Item grafico que armazena/mostra a pontuacao
//Pre-requisitos: QGraphicsTextItem QFont
#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>
#include <QFont>


class Score: public QGraphicsTextItem
{
public:
    Score(QGraphicsItem* = nullptr);

    void Increass();

private:
    int theScore;

};

#endif // SCORE_H
