//Classe de controle
//Padrao Singleton
//Pre-requisitos: QGraphicsItem QTimer QObject QGraphicsScene
//QKeyEvent QApplication Nave Slimeet Digeet ToolBox
#ifndef OVERWATCHER_H
#define OVERWATCHER_H

#include <QGraphicsItem>
#include <QTimer>
#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QApplication>


#include "../includes/nave.h"
#include "../includes/entidade.h"
#include "qelement.h" //header para heranca
#include "../includes/slimeet.h"
#include "score.h"
#include "../includes/diget.h"
#include "../includes/toolbox.h"
#include "includes/bullet.h"


class Overwatcher:   public QGraphicsScene
{
    Q_OBJECT
public:
    //metodo para obtencao do objeto
    static Overwatcher &getInstance();

    ~Overwatcher();

    void shot( int );

    void show(){ viewPtr->show(); }

    void startGame();

    //insere o elemento na posicao x,y
    bool insert( Element*, int x, int y);

    //sincroniza a posicao do mundo e da cena
    bool syncScene(QGraphicsItem*);

    void insertEny();

    void endGame();

    bool isShipDead();

    void keyPressEvent(QKeyEvent *event);
public slots:
    void actAll();

private:
    //private constructor
    Overwatcher();

    static Overwatcher* instance;

    QTimer timer; //timer

    const int entyNumMax = 20;

    const int cycleTime = 120;

    QGraphicsView* viewPtr;

    QElement<Nave>* player; //ponteiro para o jogador

    Score* myscore;

    int cont;

    QGraphicsTextItem* startMensage;

    bool isRunning;
};

#endif // OVERWATCHER_H
