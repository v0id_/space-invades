#include "score.h"

Score::Score(QGraphicsItem *p ): QGraphicsTextItem(p),theScore(0)
{
    setPlainText(QString("Score: " + QString::number(theScore)));
    setDefaultTextColor(Qt::green);
    setFont(QFont("times", 16));

}

void Score::Increass()
{
    theScore++;
    setPlainText(QString("Score: " + QString::number(theScore)));
}

