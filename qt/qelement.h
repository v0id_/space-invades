//Template para uma classe derivada de QGraphicsItem
//Usado tornar elementos inseriveis na interface
//Pre-requisitos: QGraphicsRectItem
#ifndef QELEMENT_H
#define QELEMENT_H


#include <QGraphicsRectItem>

template< class Base >
class QElement: virtual public QGraphicsRectItem, virtual public Base
{
public:
    QElement();



};


template<class Base >
QElement< Base >::QElement()
{
    setRect(0,0,50,50);

}




#endif // QELEMENT_H
