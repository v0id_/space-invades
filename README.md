# Space-Invaders

## Colaborador(es)

Vitor José Duarte Quintans

## Pre-requisitos

    Qt:
    
	> sudo apt install build-essential
	> sudo apt install qtcreator
	> sudo apt install qt5-default

   
	Para reproduzir o video, instalar vcl
	
	>sudo add-apt-repository ppa:videolan/master-daily
	>sudo apt update
	>sudo apt install vlc

## Copilação/Execução
Para fazer o programa funcionar é necessário executar a seguinte série de comandos dentro do diretório Space-Invaders:


> qmake

> make

>./Space_invaders





