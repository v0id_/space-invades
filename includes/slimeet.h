//Classe inimiga que que se move na horizontal e vertical
//Pre-requisitos: Et ToolBox
#ifndef SLIMEET_H
#define SLIMEET_H

#include "../includes/et.h"
#include "../includes/toolbox.h"

class SlimeEt : public Et
{
public:
    SlimeEt();

    virtual bool act();
};

#endif // SLIMEET_H
