//classe que ocupa os espacos vazios do mapa
//permite a movimentacao de entidades
//Pre-requisitos: Element
#ifndef AIR_H
#define AIR_H

#include "../includes/element.h"

class World;

class Air : public Element
{
public:
    Air();

    virtual ~Air() override;

    //permite a movimentacao
    virtual bool colision(Element *) override;
};

#endif // AIR_H
