//Classe Basica abstrata de todas as entidades
//define a realizacao de acoes e movimentacao
//Pre-requisitos: Element, Air
#ifndef ENTIDADE_H
#define ENTIDADE_H

#include "../includes/element.h"
#include "../includes/air.h"




class Entidade: public Element
{
public:
    Entidade( int life = 1);

    virtual ~Entidade();

    virtual bool act() = 0; //realiza acao  //PURE!!!

    //troca de posicao com o elemento em ( x , y )
    bool move(int x,int y);

    void setLife(int);

    int getLife();

    void setDano(int d);

    int getDano();

    //colide com o elemento em ( x , y )
    bool goTo(int x , int y );

    void die();


private:
    int life;
    int dano;

};

#endif // ENTIDADE_H
