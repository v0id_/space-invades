//Container para informacoes sobre cada acao no jogo
//para a futura expancao do software
//Pre-requisitos: typeinfo, string
#ifndef Action_H
#define Action_H

#include <typeinfo>
#include <string>

class Element;

using std::type_info;
using std::string;

typedef enum TipoAction { INVALID ,INSERT, DELETE, COLISION, MOVE} TipoAction;


class Action
{
public:
    Action(); //construtor padrao( inicializa como INVALID)

    Action( string , TipoAction, int agx, int agy , int = -1, int = -1 ); //defalt == sem objeto

    //metodos de acesso
    int getAgtX(){ return agtX; }
    int getAgty(){ return agtY; }
    int getObjX(){ return objX; }
    int getObjY(){ return objY; }

private:
    string tipoAgente;

    TipoAction type;

    //coordenada do agente da acao
    int agtX;
    int agtY;

    //coordenadas do objeto da acao
    int objX;
    int objY;
};

#endif // Action_H
