//Elemento controlado pelo jogador
//Pre-requisitos: Entidade
#ifndef NAVE_H
#define NAVE_H

#include "../includes/entidade.h"

class Nave : public Entidade
{
public:
    Nave( int life = 1);

    virtual bool act() override;

    //nao faz nada
    virtual bool colision( Element* ) override;
};

#endif // NAVE_H
