//classe inimiga que se move na diagonal
//So causa dano na diagonal
//Pre-requisitos: Et
#ifndef DIGET_H
#define DIGET_H

#include "../includes/et.h"

class DigEt : public Et
{
public:
    DigEt();

    //se move na diagonal
    virtual bool act() override;
};

#endif // DIGET_H
