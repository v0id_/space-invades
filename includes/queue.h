//Template para fila circular
//Pre-requisitos: nenhum
#ifndef QUEUE_H
#define QUEUE_H



template <class T>
class Queue
{
public:
    Queue(int size = 30);
    
    ~Queue();
    
    Queue( const Queue<T>& ) = delete;

    bool deQueue( T& );          
        
    bool enQueue( T& );
    
    int size();
    
    bool isEmpty();
    
    bool isFull();
    
    bool operator>>( T& val ) { return deQueue( val ); }    //operadores sobrecaregados
    bool operator<<( T& val ) { return enQueue( val ); }
 
private:
    T *ptr; 		// elements
    int capacity;   // maximum capacity of the queue
    int front;  	
    int rear;   	
    int count;  	// current size 
};


template <class T>
Queue<T>::Queue(int size)
{
    ptr = new T[size];
    capacity = size;
    front = 0;
    rear = -1;
    count = 0;
}


template <class T>
Queue<T>::~Queue()
{
	delete[] ptr;
}

template <class T>
bool Queue<T>::deQueue( T& deq_value )
{
    
    if (isEmpty()) return false;
   		
	deq_value = ptr[ front ];	 

    front = (front + 1) % capacity;
    count--;
    
    return true;
}


template <class T>
bool Queue<T>::enQueue( T& enq_value )
{
    // check for Queue overflow
    if (isFull()) return false;
  

    rear = (rear + 1) % capacity;
    ptr[rear] = enq_value;
    count++;
    
    return true;
}



template <class T>
int Queue<T>::size()
{
    return count;
}


template <class T>
bool Queue<T>::isEmpty()
{
    return (size() == 0);
}


template <class T>
bool Queue<T>::isFull()
{
    return (size() == capacity);
}





#endif // QUEUE_H
