//classe basica abstrata para todos os elementos
//define a interface basica
//Pre-requisitos: World
#ifndef ELEMENT_H
#define ELEMENT_H

#include "../includes/world.h"

class Element
{
public:
    Element(int x = -1, int y = -1);

    virtual ~Element();

    int getPosX();

    void setPosX( int x );

    int getPosY();

    void setPosY( int y );

    //metodo virtual puro que define o efeito
    //distinto para o contato com um elemento
    virtual bool colision( Element* ) = 0;

    //operador sobrecarregado para facilitar o
    //uso da colisao
    Element &operator>>=( Element* );

private:
    int posX;
    int posY;


};

#endif // ELEMENT_H
