//Classe que armazena os elementos e
//gerencia as acoes
//Padrao Singleton
//Pre-requisitos: list typeinfo queue action
#ifndef WORLD_H
#define WORLD_H

#include <list>
#include <typeinfo>


#include "../includes/queue.h"
#include "../includes/action.h"


using namespace std;

class Element;
class Entidade;

class World
{  
public:
    //metodo pra obtencao do objeto
    static World& getWorld();

    //desaloca todos os elementos
    ~World();

    //inicializa o mundo com ar
    bool airInit();

    int getSizeX() const { return sizeX; }

    int getSizeY()const{ return sizeY; }

    bool moveElement(int,int,int,int);

    bool insertElement( Element*, int, int );

    void removeElement(int , int );

    bool isOutOfWorld(int, int);

    Element *getWorldElementPtr(int,int);

    Entidade *getEntity(int);

    void actAllEnty();

    int numEnty(){ return int(entyList.size()); }

    void killAllEnty();


private:
    static World* instance;

    //construtor privado
    World();

    const int sizeX = 12;
    const int sizeY = 18;

    Element* map[12][18];

    Queue< Action > actionQueue;

    std::list< Entidade* > entyList;


};

#endif // WORLD_H
