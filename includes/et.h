//classe basica abstrata dos ets (inimigos)
//define a colisao
//Pre-requisitos: Entidade
#ifndef ET_H
#define ET_H

#include "../includes/entidade.h"

class Et : public Entidade
{
public:
    Et( int = 1);

    //causa dano a qualquer elemento nao et
    virtual bool colision( Element* );


};

#endif // ET_H
