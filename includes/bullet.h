#ifndef BULLET_H
#define BULLET_H

#include "includes/entidade.h"

class Bullet: public Entidade
{
public:
    Bullet();

    virtual bool act() override;

    virtual bool colision( Element *) override;
};

#endif // BULLET_H
