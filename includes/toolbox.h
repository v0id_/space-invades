//Classe para funcoes utilitarias
//nao pode ser instanciada
//Pre-requisitos: random
#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <random>

class ToolBox
{
public:
    static int pseudorandom(int min, int max);

private:
    ToolBox(); //construtor privado
};

#endif // TOOLBOX_H
