//Derivada de "Air" que causa dano aos ets
//segura somente para a nave
//Pre-requisitos: Air
#ifndef DANGERAIR_H
#define DANGERAIR_H

#include "../includes/air.h"


class DangerAir : public Air
{
public:
    DangerAir();

    //causa dano aos ets
    //permite que a nave se mova
    virtual bool colision(Element *) override;
};







#endif // DANGERAIR_H
